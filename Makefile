.PHONY: proto
proto:
	protoc\
	    -I ./pkg/proto/ \
	    --go_out ./pkg/proto/gen/ --go_opt paths=source_relative \
        --go-grpc_out ./pkg/proto/gen/ --go-grpc_opt paths=source_relative \
		--grpc-gateway_out ./pkg/proto/gen/ --grpc-gateway_opt paths=source_relative \
		tmantoy.proto

.PHONY: build
build:
	go build -o tmantoyd cmd/tmantoyd/main.go
