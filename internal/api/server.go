package api

import (
	"context"
	"flag"
	"fmt"
	"log"
	"net"
	"net/http"

	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	gen "gitlab.com/tmanf5/tman-toy/pkg/proto/gen"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

var (
	grpcPort = flag.Int("grpcPort", 50051, "The grpc server port")
	restPort = flag.Int("restPort", 8081, "The rest server port")
)

type server struct {
	gen.UnimplementedTManToyServer
}

func CreatDaemon() {
	// parse cli arguments
	flag.Parse()

	// grpc listens on port 50051 by default
	lis, err := net.Listen("tcp", fmt.Sprintf("localhost:%d", *grpcPort))
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	// register and serve the grpc server
	s := grpc.NewServer()

	gen.RegisterTManToyServer(s, &server{})

	go func() {
		log.Printf("grpc server listening at %v", fmt.Sprintf("localhost:%d", *grpcPort))

		if err := s.Serve(lis); err != nil {
			log.Fatalf("failed to serve: %v", err)
		}
	}()

	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	// register the rest server
	mux := runtime.NewServeMux()

	err = gen.RegisterTManToyHandlerFromEndpoint(ctx, mux, fmt.Sprintf("localhost:%d", *grpcPort),
		[]grpc.DialOption{grpc.WithTransportCredentials(insecure.NewCredentials())})
	if err != nil {
		log.Fatalf("failed to register restful server: %v", err)
	}

	// serve the rest server on port 8081 by default
	log.Printf("rest server listening at %v", fmt.Sprintf("localhost:%d", *restPort))

	if err := http.ListenAndServe(fmt.Sprintf("localhost:%d", *restPort), mux); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}
