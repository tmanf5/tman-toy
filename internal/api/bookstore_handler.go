package api

import (
	"context"
	"log"

	gen "gitlab.com/tmanf5/tman-toy/pkg/proto/gen"
)

func (s *server) CreateBook(ctx context.Context, in *gen.CreateBookRequest) (*gen.CreateBookReply, error) {
	log.Printf("Received: %v", in.GetName())

	return &gen.CreateBookReply{Message: "Received " + in.GetName()}, nil
}
