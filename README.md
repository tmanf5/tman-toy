# tman-toy

toy project for learning.

### How-to create a service from scratch

#### Create a new Go project

Use `go mod init <module-path>` to initiate a new go project. Please consult [this](https://go.dev/doc/tutorial/create-module) tutorial for more info.

#### Describe the service - using Protocol Buffers

Inside the project dir, create a proto file that describes one's service. Please consult [this](https://grpc.io/docs/languages/go/quickstart) tutorial for more info.

Read about how `protobuf` project separated from the `golang` project in [this](https://stackoverflow.com/questions/64828054/differences-between-protoc-gen-go-and-protoc-gen-go-grpc) stackoverflow thread.

#### Map REST endpoints to the service - using grpc-gateway anotation

To map REST endpoints to the service grpc calls, we can use the `grpc-gateway` anotation in our proto file. Please consult the grpc-gateway [repo](https://github.com/grpc-ecosystem/grpc-gateway) for more info.

#### Compile the proto file

Please consult [this](https://grpc.io/docs/languages/go/quickstart) tutorial and the grpc-gateway [repo](https://github.com/grpc-ecosystem/grpc-gateway) for more info regarding how to use `protoc` to compile the framework skeleton/scaffolding.

One unfotunate compile time error one might encounter is [this](https://github.com/grpc-ecosystem/grpc-gateway/issues/1065) one about grpc-gateway anotation.

### Good read

Go project standard layout: https://github.com/golang-standards/project-layout

go get & go install after Go 1.16: https://tip.golang.org/doc/go1.16#go-command

go mod commands: https://go.dev/ref/mod#go-mod-tidy

### Intersting hack

Go 1.17 SDK problem in older Jet Brain IDEs: https://youtrack.jetbrains.com/issue/GO-11588

